/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;

public class Tarea implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
    private String descripcion;
    private Date fechaAlta;
    private Date fechaFin;
    private Empleado empleado;

    public Tarea() {
    }

    public Tarea(String descripcion, Date fechaAlta, Date fechaFin, Empleado emp) {
        this.descripcion = descripcion;
        this.fechaAlta = fechaAlta;
        this.fechaFin = fechaFin;
        this.empleado = emp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    @Override
    public String toString() {
        return "Tarea{" + "id=" + id + ", descripcion=" + descripcion + ", fechaAlta=" + fechaAlta + ", fechaFin=" + fechaFin + '}';
    }

}
