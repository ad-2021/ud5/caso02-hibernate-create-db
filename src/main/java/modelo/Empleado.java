/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Empleado implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
    private String nombre;
    private String passwd;
    private String email;
    private Set<Tarea> tareas = new HashSet<Tarea>(0);

    public Empleado() {
    }

    public Empleado(String nombre, String passwd, String email) {
        this.nombre = nombre;
        this.passwd = passwd;
        this.email = email;
    }

    public Empleado(String nombre, String passwd, String email, Set<Tarea> tareas) {
        this.nombre = nombre;
        this.passwd = passwd;
        this.email = email;
        this.tareas = tareas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Tarea> getTareas() {
        return tareas;
    }

    public void setTareas(Set<Tarea> tareas) {
        this.tareas = tareas;
    }

    public void addTarea(Tarea t) {
        tareas.add(t);
    }

    public void removeTarea(Tarea t) {
        tareas.remove(t);
    }

    @Override
    public String toString() {
        return "Empleado{" + "id=" + id + ", nombre=" + nombre + ", passwd=" + passwd + ", email=" + email + '}';
    }

}
