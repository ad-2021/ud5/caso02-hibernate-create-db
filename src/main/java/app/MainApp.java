package app;

import java.util.Date;

import org.hibernate.Session;

import modelo.Empleado;
import modelo.Tarea;

public class MainApp {

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		org.jboss.logging.Logger logger = org.jboss.logging.Logger.getLogger("org.hibernate");
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);

		System.out.println("APP - GESTIÓN TAREAS");

		try {
			Session s = Conexion.getSession();
			// Creamos empleado y lo guardamos en la bd
			Empleado e = new Empleado("Juan", "1234", "juan@batoi.es");
			s.beginTransaction();
			s.save(e);
			s.getTransaction().commit();
			System.out.println("Insertado empleado de prueba! "+e.getId());

			// Creamos tarea y asociamos el empleado a dicha tarea
			// Esto hará que automáticamente el empleado disponga de la tarea en su lista.
			Tarea t1 = new Tarea("tarea1", new Date(), null, e);
			Tarea t2 = new Tarea("tarea2", new Date(), null, e);
			s.beginTransaction();
			s.save(t1);
			s.save(t2);
			s.getTransaction().commit();
			System.out.println("Insertada tarea de prueba!");
			
			// Recorremos tareas del empleado
			s.refresh(e);
			for(Tarea ta: e.getTareas()) {
				System.out.println(ta.getDescripcion());
			}
			
			Conexion.closeSession();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

}
